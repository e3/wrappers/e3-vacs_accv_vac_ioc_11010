
e3-vacs_accv_vac_ioc_11010  
======
ESS Site-specific EPICS module : vacs_accv_vac_ioc_11010

Note that the .db file vacs-accv_vac-ioc-11010.db requires that:
1. the module "vac_mfc_mks_gv50a" is loaded
1. specifically, in the device "ISrc-010:Vac-VVMC-01100"

This is not really an E3 requirement: this is a requirement based on the structure of the IOC and its controlling/controlled by relationships in CCDB.